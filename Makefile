# (C) Copyright 2016
# Urs Fässler, www.bitzgi.ch
# SPDX-License-Identifier: CC-BY-SA-4.0

handname=handout
slidename=slide
staticname=static
pdfname=gpl_mit_wtf

slidetex=$(slidename).tex
handouttex=$(handname).tex
statictex=$(staticname).tex
slidepdf=$(pdfname)_$(slidename).pdf
handoutpdf=$(pdfname)_$(handname).pdf
staticpdf=$(pdfname)_$(staticname).pdf

merged: handout slide static
	gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=$(pdfname).pdf $(handoutpdf) $(slidepdf) $(staticpdf)

handout:
	pdflatex -draftmode -halt-on-error $(handouttex) 1> /dev/null
	pdflatex -halt-on-error $(handouttex) 1> /dev/null
	mv $(handname).pdf $(handoutpdf)

slide:
	pdflatex -draftmode -halt-on-error $(slidetex) 1> /dev/null
	pdflatex -halt-on-error $(slidetex) 1> /dev/null
	mv $(slidename).pdf $(slidepdf)

static:
	pdflatex -draftmode -halt-on-error $(statictex) 1> /dev/null
	pdflatex -halt-on-error $(statictex) 1> /dev/null
	mv $(staticname).pdf $(staticpdf)

debug:
	pdflatex $(handouttex)

clean:
	rm -f *.log *.toc *.aux *.bbl *.blg *.lof *.lot *.out *.bak *.nav *.snm *.vrb

clear: 
	rm -f *.dvi *.pdf *.ps

